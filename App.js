import React,{Component} from 'react'
import { StyleSheet, Text, View,Button } from 'react-native'
import { 
  StackNavigator,
  TabNavigator, 
  DrawerNavigator 
} from 'react-navigation';


class ScreenA extends Component {
  state = {  }
  componentDidMount() {
    console.log('CDM  Screen A')
  }
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text style={{fontSize:40}}>Screen A</Text>
        {/* <Button title='Go to Screen A' onPress={()=>this.props.navigation.navigate('ScreenA')}/> */}
        <Button title='Go to Screen B' onPress={()=>this.props.navigation.navigate('ScreenB')}/>
        <Button title='Go to Screen C' onPress={()=>this.props.navigation.navigate('ScreenC')}/>
        <Button title='Go to Drawer Example' onPress={()=>this.props.navigation.navigate('Screen_DrawerExample')}/>
        <Button title='Go to Tab Example' onPress={()=>this.props.navigation.navigate('Screen_TabExample')}/>
        <Button title='Back' onPress={()=>this.props.navigation.goBack()}/>
      </View>
    )
  }
}

class ScreenB extends Component {
  state = {  }
  componentDidMount() {
    console.log('CDM  Screen B')
  }
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text style={{fontSize:40}}>Screen B</Text>
        <Button title='Go to Screen A' onPress={()=>this.props.navigation.navigate('ScreenA')}/>
        {/* <Button title='Go to Screen B' onPress={()=>this.props.navigation.navigate('ScreenB')}/> */}
        <Button title='Go to Screen C' onPress={()=>this.props.navigation.navigate('ScreenC')}/>
        <Button title='Go to Tab Example' onPress={()=>this.props.navigation.navigate('Screen_TabExample')}/>
        <Button title='Back' onPress={()=>this.props.navigation.goBack()}/>
      </View>
    )
  }
}

class ScreenC extends Component {
  state = {  }
  componentDidMount() {
    console.log('CDM  Screen C')
  }
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text style={{fontSize:40}}>Screen C</Text>
        <Button title='Go to Screen A' onPress={()=>this.props.navigation.navigate('ScreenA')}/>
        <Button title='Go to Screen B' onPress={()=>this.props.navigation.navigate('ScreenB')}/>
        <Button title='Go to Tab Example' onPress={()=>this.props.navigation.navigate('Screen_TabExample')}/>
        {/* <Button title='Go to Screen C' onPress={()=>this.props.navigation.navigate('ScreenC')}/> */}
        <Button title='Back' onPress={()=>this.props.navigation.goBack()}/>
      </View>
    )
  }
}

class ScreenDrawer extends Component {
  render(){
    return(
      <View>
        <Text>Drawer Example Screen</Text>
      </View>
    )
  }
}


class TabA extends Component{
  // console.log(props)
  componentDidMount() {
    console.log('CDM Tab A')
  }
  render(){
    let props=this.props
    return (
      <View>
        <Text>Tab A</Text>
        <Button title='Back' onPress={()=>props.navigation.navigate('Drawer')}/>
      </View>
    )
  }
}

class TabB extends Component{
  // console.log(props)
  componentDidMount() {
    console.log('CDM Tab B')
  }
  render(){
    let props=this.props
    return (
      <View>
        <Text>Tab B</Text>
        <Button title='Back' onPress={()=>props.navigation.navigate('Drawer')}/>
      </View>
    )
  }
}
 

 
const MyTab=TabNavigator(
  {
    TabA:{screen:TabA},
    TabB:{screen:TabB},
  }
)

const Screen_TabExample=TabNavigator(
  {
    // ScreenB:{screen:ScreenB},
    TabA:{screen:TabA},
    TabB:{screen:TabB},
  }
)

const MyStack=StackNavigator(
  {
    ScreenA:{screen:ScreenA},
    ScreenB:{screen:ScreenB},
    ScreenC:{screen:ScreenC},
    ScreenTab:{screen:MyTab}
  },
  {
    // initialRouteName:'ScreenB'
  }
)

const MyDrawer=DrawerNavigator(
  {
    ScreenDrawer:{screen:ScreenDrawer},
    ScreenA:{screen:ScreenA},
    ScreenB:{screen:ScreenB},
    ScreenC:{screen:ScreenC},
  }
)

const AppNavigator =StackNavigator({
  ScreenA:{screen:ScreenA},
  ScreenB:{screen:ScreenB},
  ScreenC:{screen:ScreenC},
  Screen_TabExample:{screen:Screen_TabExample},
  Screen_DrawerExample:{screen:MyDrawer},
  Screen_TabExample:{screen:Screen_TabExample},
})




export default class App extends React.Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
